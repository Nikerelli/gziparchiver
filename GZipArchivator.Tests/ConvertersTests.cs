﻿using System.Linq;
using System.Text;
using GZipTest.Infrastructure;
using GZipTest.Infrastructure.Converters;
using GZipTest.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GZipArchivator.Tests
{
    [TestClass]
    public class ConvertersTests
    {
        [TestMethod]
        public void CompressionConverterTest()
        {
            var gzip = new GZipArchiver();
            var cc = new CompressionConverter(gzip);
            var str = "Higher, stronger, faster!";
            var originData = new DataBlock(Encoding.ASCII.GetBytes(str), 3);
            var compressedData = cc.Convert(originData);
            var dc = new DecompressionConverter(gzip);
            var decompressedData = dc.Convert(compressedData);
            var decompressedSum = decompressedData.Content.Sum(x => (long) x);
            var originSum = decompressedData.Content.Sum(x => (long)x);


            Assert.IsTrue(originSum == decompressedSum && compressedData.OriginOffset == originData.OriginOffset);
        }
    }
}
