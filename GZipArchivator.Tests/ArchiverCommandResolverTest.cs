﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using GZipTest.Infrastructure.Commands;

namespace GZipArchivator.Tests
{
    [TestClass]
    public class ArchiverCommandResolverTest
    {
        private const string _path = @"\Files\FilesForResolver\";
        private const string _sourceFile = "source.txt";
        private const string _compressedFile = "compressed.txt";
        private const string _decompressedFile = "decompressed.txt";

        [TestMethod]
        [DataRow(new string[0])]
        [DataRow(null)]
        [DataRow(new string[]{"decumpress   ","  fdsfsd   ","  dfsfdsf", "  dfsfdsf" })]
        [DataRow(new string[] { "decimpress   ", "  dsfsdfsd   "})]
        [DataRow(new string[] { "decompress   " })]
        [DataRow(new string[] { "compress   ", "  fdsfsd   ", "  dfsfdsf" })]
        [DataRow(new string[] { "decumpress   ", _path + _sourceFile, _path + _compressedFile })]
        [DataRow(new string[] { "decompress   ", "32!!@#!__+$" + _path + _compressedFile, _path + _decompressedFile })]
        [DataRow(new string[] { "decompress   ", "32!!@#!__+$" + _compressedFile, _path + _decompressedFile })] //*
        [DataRow(new string[] { "decompress   ", "32!!@#!__+$" + _path + _compressedFile, _decompressedFile })]
        [DataRow(new string[] { "compress   ", @"  D:\Desktop\temp\erewan!!##@FSDF   ", @"  D:\Desktop\temp\zipi4.ed" })]
        [DataRow(new string[] { @"decompress   ", "  temp.txt   ", "  dfsfdsf" })]
        [DataRow(new string[] { "decumpress   ", "  fdsfsd   ", "  ZipZap.zap" })]
        [DataRow(new string[] { "decompress   ", @"  D:\Desktop\temp\temp@@1!.txt   ", @"  D:\Desktop\temp\ZipZap--2340--3//.zap" })]
        [ExpectedException(typeof(ArgumentException))]
        public void WrongCommandParsing(string[] args)
        {
            if(args?.Length >= 2)
            args[1] = Directory.GetCurrentDirectory() + args[1];

            if(args?.Length >= 3)
            args[2] = Directory.GetCurrentDirectory() + args[2];

            ArchiverCommand.Parse(args);
        }

        [TestMethod]
        [DataRow(new string[] { "decompress", _path + _sourceFile, _path + "i" + _compressedFile })]
        [DataRow(new string[] { "decompress", _path + _compressedFile, _path + "i" + _decompressedFile  })]
        [DataRow(new string[] { "compress", _path + _sourceFile, _path + "i" + _compressedFile })]
        [DataRow(new string[] { "compress", _path + _compressedFile, _path + "i" + _decompressedFile })]
        public void CorrectCommandParsing(string[] args)
        {
            args[1] = Directory.GetCurrentDirectory() + args[1];
            args[2] = Directory.GetCurrentDirectory() + args[2];
            var command = ArchiverCommand.Parse(args);
            Assert.IsNotNull(command);
        }
    }
}
