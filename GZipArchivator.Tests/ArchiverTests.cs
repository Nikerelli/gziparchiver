﻿using System.Text;
using GZipTest.Abstractions;
using GZipTest.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GZipArchivator.Tests
{
    [TestClass]
    public class ArchiverTests
    {
        private readonly IArchiver _archiver = new GZipArchiver();

        [TestMethod]
        [DataRow("")]
        [DataRow("                  ")]
        [DataRow("ergersdflmpohbgifjowmklcerjuvxhozvjevermklggmerkm")]
        [DataRow("efuerhv ergrejgier 32423n fperj43j4gfdij ")]
        [DataRow("!@#$%^&*())(*&^%$#!@!@#$%^&*())(*&^%$#!@!@#$%^&*())(*&^%$#!@!@#$%^&*())(*&^%$#!@!@#$%^&*())(*&^%$#!@")]
        public void CompressDecompressString(string input)
        {
            var stringBytes = Encoding.ASCII.GetBytes(input);
            var compressedStringAsBytes = _archiver.Compress(stringBytes);
            var decompressedBytes = _archiver.Decompress(compressedStringAsBytes);
            var decompressedString = Encoding.ASCII.GetString(decompressedBytes);

            Assert.IsTrue(input == decompressedString);
        }
    }
}
