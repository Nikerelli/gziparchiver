﻿using GZipTest;
using GZipTest.Abstractions;
using GZipTest.Enums;
using GZipTest.Infrastructure.Channels;
using GZipTest.Infrastructure.Commands;
using GZipTest.Infrastructure.Utils;
using GZipTest.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;

namespace GZipArchivator.Tests
{
    [TestClass]
    public class StressTestForChannels
    {
        private const string _path = @"\Files\RealFiles\";
        private const string _sourceFile = "70mb.txt";
        private const string _compressedFile = "compressed.txt";
        private const string _decompressedFile = "decompressed.txt";

        [TestMethod]
        [DataRow(10)]
        public void StreassTest(int i)
        {
            var args = new[] { "compress", $"{Directory.GetCurrentDirectory()}{_path}{_sourceFile}",  $"{Directory.GetCurrentDirectory()}{_path}{_compressedFile}"};
            var args2 = new[] { "decompress", $"{Directory.GetCurrentDirectory()}{_path}{_compressedFile}", $"{Directory.GetCurrentDirectory()}{_path}{_decompressedFile}" };
            var result = new List<bool>();
            while (i > 0)
            {
                MainFlow(args);
                MainFlow(args2);

                using (var md5 = MD5.Create())
                using (var origin = File.Open($"{Directory.GetCurrentDirectory()}{_path}{_sourceFile}", FileMode.Open))
                using (var decompressed = File.Open($"{Directory.GetCurrentDirectory()}{_path}{_decompressedFile}", FileMode.Open))
                {
                    var originhash = BitConverter.ToString(md5.ComputeHash(origin));
                    var decompressedhash = BitConverter.ToString(md5.ComputeHash(decompressed));
                    result.Add(originhash.Equals(decompressedhash));
                }

                File.Delete(args[2]);
                File.Delete(args2[2]);
                i--;
            }
            GCUtils.ForceGC(true);
            Assert.IsTrue(result.TrueForAll(x => x));
        }

        [TestMethod]
        [DataRow(5, 300)]
        public void StreassGigabyteTest(int i,int timesAdd4mb)
        {
            var sourceFilePath = $"{Directory.GetCurrentDirectory()}{_path}\\GigabyteMonster.txt";
            File.Copy($"{Directory.GetCurrentDirectory()}{_path}{_sourceFile}",sourceFilePath);

            while (timesAdd4mb > 0)
            {
                Add4MB($"{sourceFilePath}");
                timesAdd4mb--;
            }

            var args = new[] { "compress", $"{sourceFilePath}", $"{Directory.GetCurrentDirectory()}{_path}{_compressedFile}" };
            var args2 = new[] { "decompress", $"{Directory.GetCurrentDirectory()}{_path}{_compressedFile}", $"{Directory.GetCurrentDirectory()}{_path}{_decompressedFile}" };
            var result = new List<bool>();
            while (i > 0)
            {
                MainFlow(args);
                MainFlow(args2);

                using (var md5 = MD5.Create())
                using (var origin = File.Open($"{Directory.GetCurrentDirectory()}{_path}{_sourceFile}", FileMode.Open))
                using (var decompressed = File.Open($"{Directory.GetCurrentDirectory()}{_path}{_decompressedFile}", FileMode.Open))
                {
                    var originhash = BitConverter.ToString(md5.ComputeHash(origin));
                    var decompressedhash = BitConverter.ToString(md5.ComputeHash(decompressed));
                    result.Add(originhash.Equals(decompressedhash));
                }

                File.Delete(args2[2]);
                File.Delete(args[2]);
                Add4MB($"{sourceFilePath}");
                i--;
            }
            File.Delete(sourceFilePath);
            GCUtils.ForceGC(true);
            Assert.IsTrue(result.TrueForAll(x => x));
        }

        private void Add4MB(string path)
        {
            using (var fs = new FileStream(path, FileMode.Open))
            {
                var bytes = new byte[4000000];
                fs.Read(bytes, 0, bytes.Length);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(bytes, 0, bytes.Length);
            }
        }

        private static int MainFlow(string[] args)
        {
            var command = ArchiverCommand.Parse(args);
            var config = new ArchivatorChannelConfig
            {
                DestinationPath = command.DestinationPath,
                SourcePath = command.SourcePath,
                DefaultBufferSize = 8388608
            };

            if (command.Type == ArchivationType.Compress)
            {
                using (ConcurrentChannel<DataBlock, DataBlock> channel = new ConcurrentCompressionChannel(config))
                {
                    channel.StartTransferData();
                }
            }
            else
            {
                using (ConcurrentChannel<DataBlock, DataBlock> channel = new ConcurrentDecompressionChannel(config))
                {
                    channel.StartTransferData();
                }
            }

            return 1;
        }
    }
}
