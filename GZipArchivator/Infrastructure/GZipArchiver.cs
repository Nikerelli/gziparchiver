﻿using System.IO;
using System.IO.Compression;
using GZipTest.Abstractions;

namespace GZipTest.Infrastructure
{
    /// <inheritdoc cref="IArchiver"/>
    public class GZipArchiver : IArchiver
    {
        /// <inheritdoc cref="IArchiver.Compress"/>
        public byte[] Compress(byte[] content)
        {
            using (var ms = new MemoryStream())
            {
                using (var gzipStream = new GZipStream(ms, CompressionMode.Compress))
                {
                    gzipStream.Write(content, 0, content.Length);
                }

                return ms.ToArray();
            }
        }

        /// <inheritdoc cref="IArchiver.Decompress"/>
        public byte[] Decompress(byte[] content)
        {
            using (var msOutput = new MemoryStream())
            using (var msInput = new MemoryStream(content))
            using (var gzipStream = new GZipStream(msInput, CompressionMode.Decompress))
            {
                gzipStream.CopyTo(msOutput);
                return msOutput.ToArray();
            }
        }
    }
}
