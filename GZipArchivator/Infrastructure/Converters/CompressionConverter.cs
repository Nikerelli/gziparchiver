﻿using GZipTest.Abstractions;
using GZipTest.Models;

namespace GZipTest.Infrastructure.Converters
{
    public class CompressionConverter : IDataConverter<DataBlock, DataBlock>
    {
        private readonly IArchiver _archiver;

        public CompressionConverter(IArchiver archiver)
        {
            _archiver = archiver;
        }

        public DataBlock Convert(DataBlock input)
        {
            var compressedData = _archiver.Compress(input.Content);
            return new DataBlock(compressedData, input.OriginOffset);
        }
    }
}
