﻿using GZipTest.Abstractions;
using GZipTest.Models;

namespace GZipTest.Infrastructure.Converters
{
    public class DecompressionConverter : IDataConverter<DataBlock, DataBlock>
    {
        private readonly IArchiver _archiver;

        public DecompressionConverter(IArchiver archiver)
        {
            _archiver = archiver;
        }

        public DataBlock Convert(DataBlock input)
        {
            var decompressed = _archiver.Decompress(input.Content);
            return new DataBlock(decompressed, input.OriginOffset);
        }
    }
}
