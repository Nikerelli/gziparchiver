﻿using System;
using System.IO;
using GZipTest.Abstractions;
using GZipTest.Infrastructure.Utils;
using GZipTest.Models;

namespace GZipTest.Infrastructure.Streams
{
    /// <inheritdoc cref="IStreamReader{TData}"/>
    public class DataBlockStreamReader : IStreamReader<DataBlock>
    {
        private readonly FileStream BaseStream;

        public bool CanRead => BaseStream.CanRead;
        public long FileSize { get; }

        private readonly int _defaultBlockSize;
        private readonly string _path;

        public DataBlockStreamReader(string path, int defaultDefaultBlockSize)
        {
            _path = path;
            _defaultBlockSize = defaultDefaultBlockSize;
            BaseStream = new FileStream(_path, FileMode.Open, FileAccess.Read, FileShare.Read);
            FileSize = new FileInfo(path).Length;
        }

        public DataBlock Read()
        {
            try
            {
                if (BaseStream.Position == BaseStream.Length)
                    return null;

                int blockSize = _defaultBlockSize;
                //var availableBlockSize = GCUtils.AvailableFreeMemory();
                //if (_defaultBlockSize > availableBlockSize)
                //{
                //    blockSize = (int)availableBlockSize;
                //}
                var bytesLeft = BaseStream.Length - BaseStream.Position;
                if (bytesLeft < blockSize)
                {
                    blockSize = (int)bytesLeft;
                }

                var offset = BaseStream.Position;
                var contentAsByte = new byte[blockSize];
                BaseStream.Read(contentAsByte, 0, contentAsByte.Length);

                return new DataBlock(contentAsByte, offset);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}\n{ex.StackTrace}");
                throw ex;
            }
        }

        public void Dispose()
        {
            BaseStream.Close();
        }
    }
}
