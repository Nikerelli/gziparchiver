﻿using System;
using System.IO;
using GZipTest.Abstractions;
using GZipTest.Models;

namespace GZipTest.Infrastructure.Streams
{
    /// <inheritdoc cref="IStreamWriter{TData}"/>
    public class CompressionStreamWriter : IStreamWriter<DataBlock>
    {
        private readonly FileStream BaseStream;

        public CompressionStreamWriter(string path)
        {
            BaseStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);
        }

        public void Dispose()
        {
            BaseStream.Close();
        }

        public int Write(DataBlock data)
        {
            try
            {
                // writes block length when it compressed
                var lengthAsBytes = BitConverter.GetBytes(data.Content.Length);
                BaseStream.Write(lengthAsBytes, 0, lengthAsBytes.Length);

                // writes origin offset
                var offsetAsBytes = BitConverter.GetBytes(data.OriginOffset);
                BaseStream.Write(offsetAsBytes, 0, offsetAsBytes.Length);

                // writes block content
                BaseStream.Write(data.Content, 0, data.Content.Length);
                return data.Content.Length + offsetAsBytes.Length + lengthAsBytes.Length;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}\n{ex.StackTrace}");
                throw ex;
            }
        }
    }
}
