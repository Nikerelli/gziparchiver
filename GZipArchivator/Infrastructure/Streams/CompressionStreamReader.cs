﻿using System;
using System.IO;
using GZipTest.Abstractions;
using GZipTest.Infrastructure.Utils;
using GZipTest.Models;

namespace GZipTest.Infrastructure.Streams
{
    /// <inheritdoc cref="IStreamReader{TData}"/>
    public class CompressionStreamReader : IStreamReader<DataBlock>
    {
        private readonly FileStream BaseStream;

        public bool CanRead => BaseStream.CanRead;
        public long FileSize { get; }

        public CompressionStreamReader(string path)
        {
            BaseStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            FileSize = new FileInfo(path).Length;
        }

        public DataBlock Read()
        {
            try
            {
                if (BaseStream.Position == BaseStream.Length)
                    return null;

                // reads block length as header
                var blockLengthAsByte = new byte[Constants.MetadataIntSize];
                BaseStream.Read(blockLengthAsByte, 0, blockLengthAsByte.Length);
                var blockLength = BitConverter.ToInt32(blockLengthAsByte, 0);


                // reads block number as header
                var originOffsetAsByte = new byte[Constants.MetadataIntSize * 2];
                BaseStream.Read(originOffsetAsByte, 0, originOffsetAsByte.Length);
                var blockOffset = BitConverter.ToInt64(originOffsetAsByte, 0);

                int blockSize = blockLength;
                var availableBlockSize = GCUtils.AvailableFreeMemory();
                if (blockSize > availableBlockSize)
                {
                    throw new OutOfMemoryException($"There is no enough memory to read this block {blockSize} bytes");
                }

                // reads content
                var compressedContent = new byte[blockLength];
                BaseStream.Read(compressedContent, 0, compressedContent.Length);

                return new DataBlock(compressedContent, blockOffset);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}\n{ex.StackTrace}");
                BaseStream.Seek(-12, SeekOrigin.Current);
                throw ex;
            }
        }

        public static int GetBlocksCount(string path)
        {
            using (var baseStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var totalBlockAstByte = new byte[Constants.MetadataIntSize];
                baseStream.Seek(0, SeekOrigin.Begin);
                baseStream.Read(totalBlockAstByte, 0, totalBlockAstByte.Length);
                return BitConverter.ToInt32(totalBlockAstByte, 0);
            }
        }

        public void Dispose()
        {
            BaseStream.Close();
        }
    }
}
