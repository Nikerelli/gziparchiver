﻿using System;
using System.IO;
using GZipTest.Abstractions;
using GZipTest.Models;

namespace GZipTest.Infrastructure.Streams
{
    /// <inheritdoc cref="IStreamWriter{TData}"/>
    public class DataBlockStreamWriter : IStreamWriter<DataBlock>
    {
        private readonly FileStream BaseStream;

        public DataBlockStreamWriter(string path)
        {
            BaseStream = new FileStream(path, FileMode.OpenOrCreate);
        }

        public int Write(DataBlock data)
        {
            try
            {
                BaseStream.Position = 0;
                BaseStream.Seek(data.OriginOffset, SeekOrigin.Begin);

                BaseStream.Write(data.Content, 0, data.Content.Length);

                return data.Content.Length;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}\n{ex.StackTrace}");
                throw ex;
            }
        }

        public void Dispose()
        {
            BaseStream.Close();
        }
    }
}
