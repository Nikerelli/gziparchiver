﻿using System;
using System.Diagnostics;
using Microsoft.VisualBasic.Devices;

namespace GZipTest.Infrastructure.Utils
{
    public static class GCUtils
    {
        /// <summary>
        /// Force garbage collection of the all generations.
        /// </summary>
        /// <param name="message">Output to console the information about allocated memory before and after GC collection.</param>
        public static void ForceGC(bool message = false)
        {
            if (message)
            {
                Console.WriteLine($"Memory before full GC: {GC.GetTotalMemory(false)} bytes.");
                GC.Collect();
                Console.WriteLine($"Memory after full GC: {GC.GetTotalMemory(true)} bytes.");
            }
            else
            {
                GC.GetTotalMemory(true);
            }
        }

        public static float GetCpuLoad()
        {
            var cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            return cpuCounter.NextValue();
        }

        public static float AvailableFreeMemory()
        {
            var info = new ComputerInfo();
            return info.AvailableVirtualMemory;
        }

        /// <summary/>
        /// <returns>System information</returns>
        public static int GetProcessorsCount()
        {
            int availableCores = 0;
            foreach (var item in new System.Management.ManagementObjectSearcher("Select * from Win32_Processor").Get())
            {
                availableCores += int.Parse(item["NumberOfCores"].ToString());
            }

            return availableCores;
        }
    }
}
