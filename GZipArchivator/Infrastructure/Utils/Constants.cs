﻿namespace GZipTest.Infrastructure.Utils
{
    public static class Constants
    {
        public const int MetadataIntSize = 4; // Metadata size
        public const ulong MemoryRateUsage = 30; // Limit of RAM usage in percents/ If it higher than 30%, then GC will be triggered

        public const long Kb = 1024;
        public const long Mb = 1048576;
        public const long Gb = 1073741824;

    }
}
