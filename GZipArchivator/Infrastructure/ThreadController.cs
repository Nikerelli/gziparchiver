﻿using GZipTest.Infrastructure.Utils;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;

namespace GZipTest.Infrastructure
{
    /// <summary>
    ///     Allow to run task in parallel mode
    /// </summary>
    public class ThreadController
    {
        public int AvailableThreads { get; }
        private readonly ConcurrentDictionary<int, Thread> _runningThreads;
        private readonly ConcurrentQueue<Action> _executionQueue;
        private readonly AutoResetEvent _locker = new AutoResetEvent(false);

        private bool _isRunning;

        public ThreadController()
        {
            AvailableThreads = GCUtils.GetProcessorsCount();
            _runningThreads = new ConcurrentDictionary<int, Thread>();
            _executionQueue = new ConcurrentQueue<Action>();
        }

        /// <summary>
        ///     Runs task parallel
        /// </summary>
        /// <param name="action">Task</param>
        public void StartParallel(Action action)
        {
            _isRunning = true;
            for (int i = 0; i < AvailableThreads; i++)
            {
                _executionQueue.Enqueue(action);
                var thread = new Thread(Execute);
                if(_runningThreads.TryAdd(thread.ManagedThreadId, thread))
                {
                    thread.Start();
                }
            }

            _locker.WaitOne();
        }

        /// <summary>
        ///     Interrupt current thread controller iteration
        /// </summary>
        public void Interrupt()
        {
            try
            {
                while (!_executionQueue.IsEmpty)
                {
                    _executionQueue.TryDequeue(out Action result);
                }
            }
            finally
            {
                _isRunning = false;
            }
        }

        /// <summary>
        ///     Execute every task in a queue
        /// </summary>
        private void Execute()
        {
            try
            {
                while (_executionQueue.TryDequeue(out Action task))
                {
                    task.Invoke();
                }
            }
            catch (OperationCanceledException ex)
            {
                Console.WriteLine($"Thread {Thread.CurrentContext.ContextID} has been cancelled");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            finally
            {
                _runningThreads.TryRemove(Thread.CurrentThread.ManagedThreadId, out Thread current);

                if (!_isRunning || (_executionQueue.IsEmpty && _runningThreads.IsEmpty))
                {
                    _locker?.Set();
                }

                Thread.CurrentThread.Abort();
            }
        }
    }
}
