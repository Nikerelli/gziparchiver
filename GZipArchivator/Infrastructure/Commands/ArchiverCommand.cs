﻿using GZipTest.Enums;
using System;
using System.IO;

namespace GZipTest.Infrastructure.Commands
{
    /// <summary/>
    public class ArchiverCommand
    {
        /// <summary>
        ///     Archive operation type
        /// </summary>
        public readonly ArchivationType Type;

        /// <summary/>
        public readonly string DestinationPath;

        /// <summary/>
        public readonly string SourcePath;

        public ArchiverCommand(
            ArchivationType type,
            string sourceFilePath,
            string destinationFilePath)
        {
            Type = type;
            DestinationPath = destinationFilePath;
            SourcePath = sourceFilePath;
        }

        /// <summary/>
        public static ArchiverCommand Parse(string[] args)
        {
            try
            {
                if (args == null)
                {
                    throw new ArgumentException($"Input string cannot be null or empty");
                }

                if (args.Length != 3)
                {
                    throw new ArgumentException("Command has less or more than 3 keywords");
                }

                if (!Enum.TryParse(args[0], true, out ArchivationType type))
                {
                    throw new ArgumentException($"Input parameter:{args[0]}.Expected values are \"compress\"|\"decompress\".");
                }

                var inputFilePath = args[1];
                if (inputFilePath.IndexOfAny(Path.GetInvalidPathChars()) != -1)
                    throw new ArgumentException($"Invalid input file parameter: {inputFilePath}. It must be correct file path.");
                if (!File.Exists(inputFilePath))
                    throw new ArgumentException($"Input file with path {inputFilePath} is not exists. Nothing to compress|decompress.");
                var fileInfo = new FileInfo(inputFilePath);
                if(fileInfo.Length == 0)
                    throw new ArgumentException($"Source file {inputFilePath} is empty. Nothing to compress|decompress.");

                var outputFilePath = args[0];
                if (outputFilePath.IndexOfAny(Path.GetInvalidPathChars()) != -1)
                    throw new ArgumentException($"Invalid output file parameter: {outputFilePath}. It must be correct file path.");
                if (File.Exists(outputFilePath))
                    throw new ArgumentException($"Output file with path {outputFilePath} is already exists. Remove it or choose another name for output file.");

                return new ArchiverCommand(type, args[1], args[2]);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message, ex);
            }
        }
    }
}
