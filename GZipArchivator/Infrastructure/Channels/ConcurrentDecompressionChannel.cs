﻿using GZipTest.Abstractions;
using GZipTest.Infrastructure.Converters;
using GZipTest.Infrastructure.Streams;
using GZipTest.Models;

namespace GZipTest.Infrastructure.Channels
{
    /// <summary>
    ///     Decompress data from one file to another
    /// </summary>
    public class ConcurrentDecompressionChannel : ConcurrentChannel<DataBlock, DataBlock>
    {
        private readonly int _defaultBlockSize;

        public ConcurrentDecompressionChannel(ArchivatorChannelConfig config)
            : base()
        {
            _writingStream = new DataBlockStreamWriter(config.DestinationPath);
            _readingStream = new CompressionStreamReader(config.SourcePath); 
            _converter = new DecompressionConverter(new GZipArchiver());
            _defaultBlockSize = config.DefaultBufferSize;
        }
    }
}
