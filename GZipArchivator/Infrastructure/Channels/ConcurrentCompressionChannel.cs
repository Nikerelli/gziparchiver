﻿using System.IO;
using GZipTest.Abstractions;
using GZipTest.Infrastructure.Converters;
using GZipTest.Infrastructure.Streams;
using GZipTest.Models;

namespace GZipTest.Infrastructure.Channels
{
    /// <summary>
    ///     Compress data from one file to another
    /// </summary>
    public class ConcurrentCompressionChannel : ConcurrentChannel<DataBlock, DataBlock>
    {
       public ConcurrentCompressionChannel(ArchivatorChannelConfig config) 
       {
           _readingStream = new DataBlockStreamReader(config.SourcePath, config.DefaultBufferSize);
           _writingStream = new CompressionStreamWriter(config.DestinationPath);
            _converter = new CompressionConverter(new GZipArchiver());
       }
    }
}
