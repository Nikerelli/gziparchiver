﻿namespace GZipTest.Models
{
    public class DataBlock
    {
        public readonly byte[] Content;
        public readonly long OriginOffset;

        public DataBlock(byte[] content, long originOffset)
        {
            Content = content;
            OriginOffset = originOffset;
        }
    }
}
