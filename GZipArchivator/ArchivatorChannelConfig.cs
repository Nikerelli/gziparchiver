﻿namespace GZipTest
{
    public class ArchivatorChannelConfig
    {
        public int DefaultBufferSize { get; set; }
        public string SourcePath { get; set; }
        public string DestinationPath { get; set; }
    }
}
