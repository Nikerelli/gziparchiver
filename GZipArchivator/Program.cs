﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;
using GZipTest.Abstractions;
using GZipTest.Enums;
using GZipTest.Infrastructure.Channels;
using GZipTest.Infrastructure.Commands;
using GZipTest.Infrastructure.Utils;
using GZipTest.Models;

namespace GZipTest
{
    class Program
    {
        private static ConcurrentChannel<DataBlock, DataBlock> _concurrentChannel;

        static int Main(string[] args)
        {
#if DEBUG
            //args = new[] { "compress", @"D:\Desktop\temp\temp.txt", @"D:\Desktop\temp\temp1.txt" };
            args = new[] { "decompress", @"D:\Desktop\temp\temp1.txt", @"D:\Desktop\temp\temp2.txt" };
            File.Delete(args[2]);
#endif
            try
            {
                return MainFlow(args);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
            finally
            {
                _concurrentChannel?.Dispose();
                Console.WriteLine("Program will be exit in 5 seconds");
                Thread.Sleep(5000);
            }
        }

        private static int MainFlow(string[] args)
        {
            var watcher = new Stopwatch();
            watcher.Start();

            AddCancelKeyHandler();
            AddUnhandledExceptionHandler();
            var command = ArchiverCommand.Parse(args);
            var config = new ArchivatorChannelConfig
            {
                DestinationPath = command.DestinationPath,
                SourcePath = command.SourcePath,
                DefaultBufferSize = int.Parse(ConfigurationManager.AppSettings["thredBufferSize"])
            };
            if (command.Type == ArchivationType.Compress)
            {
                _concurrentChannel = new ConcurrentCompressionChannel(config);
            }
            else
            {
                _concurrentChannel = new ConcurrentDecompressionChannel(config);
            }

            using (_concurrentChannel)
            {
                _concurrentChannel.StartTransferData();
            }

            watcher.Stop();
            Console.WriteLine((decimal)watcher.ElapsedMilliseconds / 1000 + " seconds passed");
            GCUtils.ForceGC();
            return 1;
        }

        private static void AddUnhandledExceptionHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += (o, e) => { Console.Error.WriteLine(e); };
            Thread.Sleep(2000);
        }

        private static void AddCancelKeyHandler()
        {
            Console.CancelKeyPress += (o, e) =>
            {
                _concurrentChannel?.Dispose();
                e.Cancel = true;
            };
        }
    }
}
