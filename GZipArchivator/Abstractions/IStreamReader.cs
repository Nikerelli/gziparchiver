﻿using System;

namespace GZipTest.Abstractions
{
    /// <summary>
    ///     Stream that reads the defined source
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public interface IStreamReader<TData> : IDisposable
    {
        /// <summary/>
        /// <returns>Read data</returns>
        TData Read();

        bool CanRead { get; }
        long FileSize { get; }
    }
}
