﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using GZipTest.Infrastructure;

namespace GZipTest.Abstractions
{
    /// <summary>
    ///     Create channel between two defined files for conversion
    ///     by operating defined data types
    /// </summary>
    /// <typeparam name="TInput">Data model that will be read from source file</typeparam>
    /// <typeparam name="TOutput">Data model that will be written to the destination file</typeparam>
    public abstract class ConcurrentChannel<TInput, TOutput> : IDisposable
    {
        protected IDataConverter<TInput, TOutput> _converter;
        protected IStreamWriter<TOutput> _writingStream;
        protected IStreamReader<TInput> _readingStream;

        protected readonly ConcurrentQueue<TInput> _originBlocks = new ConcurrentQueue<TInput>();
        protected readonly ConcurrentQueue<TOutput> _convertedBlocks = new ConcurrentQueue<TOutput>();
        protected readonly ThreadController _threadPool;

        private readonly CancellationTokenSource _tokenSource = new CancellationTokenSource();
        private Mutex _writingThread = new Mutex(false);
        private Mutex _readingThread = new Mutex(false);

        public ConcurrentChannel()
        {
            _threadPool = new ThreadController();
        }

        /// <summary>
        ///     Starting data transfer between two files
        /// </summary>
        public virtual void StartTransferData()
        {
            _threadPool.StartParallel(TransferData);
        }

        protected virtual void TransferData()
        {
            while (!_tokenSource.Token.IsCancellationRequested)
            {
                try
                {
                    _readingThread.WaitOne();
                    if (_readingStream.CanRead)
                    {
                        var data = _readingStream.Read();
                        if (data == null)
                        {
                            return;
                        }

                        _originBlocks.Enqueue(data);
                    }
                    else
                    {
                        return;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw ex;
                }
                finally
                {
                    _readingThread.ReleaseMutex();
                }

                if (_originBlocks.TryDequeue(out TInput originBlock))
                {
                    var compressedData = _converter.Convert(originBlock);
                    _convertedBlocks.Enqueue(compressedData);
                }
                else
                {
                    return;
                }

                try
                {
                    _writingThread.WaitOne();

                    if (_convertedBlocks.TryDequeue(out TOutput convertedBlock))
                    {
                        _writingStream.Write(convertedBlock);
                    }
                    else
                    {
                        return;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw ex;
                }
                finally
                {
                    _writingThread.ReleaseMutex();
                }
            }
            _tokenSource.Token.ThrowIfCancellationRequested();
        }

        public void Dispose()
        {
            _writingStream?.Dispose();
            _readingStream?.Dispose();
            _threadPool?.Interrupt();
            _tokenSource?.Dispose();
        }
    }
}
