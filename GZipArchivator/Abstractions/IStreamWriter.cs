﻿using System;

namespace GZipTest.Abstractions
{
    /// <summary>
    ///     Stream that writes data into the defined source
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public interface IStreamWriter<TData> : IDisposable
    {
        /// <summary/>
        /// <param name="data">Data write to</param>
        int Write(TData data);
    }
}
