﻿namespace GZipTest.Abstractions
{
    /// <summary>
    ///     Allows compress and decompress files
    /// </summary>
    public interface IArchiver
    {
        /// <summary/>
        /// <param name="content">content to compress</param>
        /// <returns></returns>
        byte[] Compress(byte[] content);

        /// <summary/>
        /// <param name="content">content to decompress</param>
        /// <param name="initialBlockSize">decompressed content size</param>
        /// <returns></returns>
        byte[] Decompress(byte[] content);
    }
}
