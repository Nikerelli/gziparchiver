﻿namespace GZipTest.Abstractions
{
    /// <summary/>
    public interface IDataConverter<TSource, TDest>
    {
        /// <summary/>
        TDest Convert(TSource input);
    }
}
